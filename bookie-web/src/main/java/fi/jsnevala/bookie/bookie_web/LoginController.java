package fi.jsnevala.bookie.bookie_web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * Handles requests for login pages
 *
 */
@Controller
public class LoginController {
	
	@RequestMapping(value="/login", method = RequestMethod.GET) 
	public String login(ModelMap model) {
		model.addAttribute("message", "You may login with you own credentials or with guest access (<i>guest</i> / <i>guest</i>)"); 
		return "login";
	}
	
	@RequestMapping(value="/loginfailed", method = RequestMethod.GET)
	public String loginerror(ModelMap model) { 
		model.addAttribute("error", "true");
		return "login"; 
	}
 
	@RequestMapping(value="/loggedout", method = RequestMethod.GET)
	public String loggedout(ModelMap model) {
		model.addAttribute("message", "You have been logged out.");
		return "login";
	}

	@RequestMapping(value="/logout", method = RequestMethod.GET) 
	public String logout(ModelMap model) {
		return "redirect:/j_spring_security_logout";
	}
}

package fi.jsnevala.bookie.bookie_web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import fi.jsnevala.bookie.bookie_data.User;
import fi.jsnevala.bookie.bookie_services.Bookieservice;
import fi.jsnevala.bookie.bookie_services.NotFoundException;
 
/**
 * 
 * Handles requests user pages
 *
 */
@Controller
public class UserController {
 
	@Autowired
	Bookieservice bookieService;

	@RequestMapping(value="/user", method = RequestMethod.GET) 
	public ModelAndView allUsers() {
		List<User> allUsers = bookieService.getAllUsers();
		ModelAndView mav = new ModelAndView("allUsers");
		mav.addObject(allUsers);
		return mav;
	}

	@RequestMapping(value="/user/{userName}", method = RequestMethod.GET)
	public ModelAndView getSingleUser(@PathVariable String userName) {
		ModelAndView mav = new ModelAndView();
		try {
			mav.setViewName("singleUser");
			User user = bookieService.getUser(userName);
			mav.addObject("user", user);
		} catch (NotFoundException e) {
			mav.setViewName("error");
			mav.addObject("message", "User " + userName + " not found!");
		}
		return mav;
	}
}

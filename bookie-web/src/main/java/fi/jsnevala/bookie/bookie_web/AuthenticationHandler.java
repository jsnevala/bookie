package fi.jsnevala.bookie.bookie_web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

/**
 * 
 * Redirects to user page after adding a new user
 *
 */
public class AuthenticationHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
	        Authentication authentication) throws ServletException, IOException {
		String username = authentication.getName();
		String targetUrl = "/user/" + username;
		getRedirectStrategy().sendRedirect(request, response, targetUrl);

	}
}

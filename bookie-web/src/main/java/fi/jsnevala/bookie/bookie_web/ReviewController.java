package fi.jsnevala.bookie.bookie_web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import fi.jsnevala.bookie.bookie_data.Review;
import fi.jsnevala.bookie.bookie_services.Bookieservice;
import fi.jsnevala.bookie.bookie_services.NotFoundException;
 
/**
 * 
 * Handles requests for review pages
 *
 */
@Controller
public class ReviewController {
 
	@Autowired
	Bookieservice bookieService;

	@RequestMapping(value="/review", method = RequestMethod.GET) 
	public ModelAndView allReviews() {
		List<Review> allReviews = bookieService.getAllReviews();
		ModelAndView mav = new ModelAndView("allReviews");
		mav.addObject(allReviews);
		return mav;
	}

	@RequestMapping(value="/review/{reviewId}", method = RequestMethod.GET)
	public ModelAndView getSingleReview(@PathVariable Integer reviewId) {
		ModelAndView mav = new ModelAndView();
		try {
			mav.setViewName("singleReview");
			Review review = bookieService.getReview(reviewId);
			mav.addObject("review", review);	
		} catch (NotFoundException e) {
			mav.setViewName("error");
			mav.addObject("message", "Review with id " + reviewId + " not found!");
		}
		return mav;
	}
}

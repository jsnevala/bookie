package fi.jsnevala.bookie.bookie_web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles requests for the application home, error and about pages.
 */
@Controller
public class BaseController {
		
	@RequestMapping(value="/", method = RequestMethod.GET) 
	public ModelAndView home() {
		return new ModelAndView("home");
	}

	@RequestMapping(value="/404", method = RequestMethod.GET) 
	public ModelAndView pageNotFound() {
		return new ModelAndView("error", "message", "The page you requested was not found!");
	}

	@RequestMapping(value="/error", method = RequestMethod.GET) 
	public ModelAndView error() {
		return new ModelAndView("error", "message", "An unxpected error just happened, please try again!");
	}

	@RequestMapping(value="/about", method = RequestMethod.GET) 
	public ModelAndView about() {
		return new ModelAndView("about");
	}

}





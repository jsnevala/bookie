package fi.jsnevala.bookie.bookie_web;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fi.jsnevala.bookie.bookie_data.User;
import fi.jsnevala.bookie.bookie_services.Bookieservice;
import fi.jsnevala.bookie.bookie_services.UsernameAlreadyExistsException;

/**
 * 
 * Handles requests add user form
 *
 */
@Controller
@RequestMapping(value="/adduser")
public class UserFormController {

	@Autowired
	Bookieservice bookieService;

    @Autowired
    AuthenticationManager authenticationManager;

	@InitBinder
	public void setAllowedFields(WebDataBinder dataBinder) {
		dataBinder.setDisallowedFields("id");
	}

	@RequestMapping(method = RequestMethod.GET)
	public String setupAddUserForm(ModelMap model) {
		model.addAttribute("user", new User());
		return "addUser";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String processSubmit(@Valid @ModelAttribute User user, BindingResult result, HttpServletRequest request) {
		String returnView = "redirect:/error";

		if (result.hasFieldErrors("username") || 
			result.hasFieldErrors("location") ||
			result.hasFieldErrors("password")) {
			returnView = "addUser";
		} else {
	    	try {
	    		String plainTextPassword = user.getPassword();
	    		
				user.setJoinDate(new Date());		
				ShaPasswordEncoder enc = new ShaPasswordEncoder();
				user.setPassword(enc.encodePassword(user.getPassword(), user.getUsername()));
	    		
				bookieService.addUser(user);

				authenticateUserAndSetSession(user.getUsername(), plainTextPassword, request);
	            returnView = "redirect:/user/" + user.getUsername();
	    	} catch (UsernameAlreadyExistsException e) {
	    		result.rejectValue("username", "exists", "Username already exists");
	    		returnView = "addUser";    		
	    	} catch (Exception e) {
	    		/* redirects to /error */
	    	}
		}
    	return returnView;
	}	
	
	private Boolean authenticateUserAndSetSession(String username, String password, HttpServletRequest request) 
		throws Exception {
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
		
		// generate session if one doesn't exist
		request.getSession();
		
		token.setDetails(new WebAuthenticationDetails(request));
		Authentication authenticatedUser = authenticationManager.authenticate(token);
		SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
		return true;
	}
}

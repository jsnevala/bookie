package fi.jsnevala.bookie.bookie_web;

import fi.jsnevala.bookie.bookie_data.Book;
import fi.jsnevala.bookie.bookie_data.Review;

/**
 * 
 * Mediates information of a new review from UI to service layer
 *
 */
public class ReviewForm {

	private Review review;
	private Book book;
	private String username;
	
	public ReviewForm() {	
	}
	
	public Review getReview() {
		return review;
	}
	public void setReview(Review review) {
		this.review = review;
	}

	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username.trim();
	}
}

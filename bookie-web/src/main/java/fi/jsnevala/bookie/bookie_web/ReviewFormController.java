package fi.jsnevala.bookie.bookie_web;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fi.jsnevala.bookie.bookie_data.Book;
import fi.jsnevala.bookie.bookie_data.Review;
import fi.jsnevala.bookie.bookie_data.User;
import fi.jsnevala.bookie.bookie_services.Bookieservice;

/**
 * 
 * Handles requests add review form
 *
 */
@Controller
@RequestMapping(value = "/addreview")
public class ReviewFormController {

	@Autowired
	Bookieservice bookieService;

	@InitBinder
	public void setAllowedFields(WebDataBinder dataBinder) {
		dataBinder.setDisallowedFields("id");
	}

	private ArrayList<Double> getRatingScale() {
		ArrayList<Double> ratingScale = new ArrayList<Double>();
		Double rating = 0.0;
		while (rating <= 5.0) {
			ratingScale.add(rating);
			rating += 0.5;
		}
		return ratingScale;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String setupAddReviewForm(ModelMap model, Principal principal) {
		String username = principal.getName();
		ReviewForm form = new ReviewForm();		
		form.setUsername(username);
		model.addAttribute("reviewForm", form);
		model.addAttribute("ratingScale", getRatingScale());
		return "addReview";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String processSubmit(@ModelAttribute ReviewForm reviewForm, BindingResult result, ModelMap model) {
		new ReviewFormValidator().validate(reviewForm, bookieService, result);
		String returnView = "redirect:/error";
		if (result.hasErrors()) {
			model.addAttribute("ratingScale", getRatingScale());
			returnView = "addReview";
		} else {
			Book book = new Book(reviewForm.getBook().getTitle(), 
				                 reviewForm.getBook().getAuthor(), 
				                 reviewForm.getBook().getYear(), 
					             reviewForm.getBook().getLanguage());

			Review review = new Review(reviewForm.getReview().getComments(), 
									   reviewForm.getReview().getRating(), 
									   new Date());
			
			try {
				User user = bookieService.getUser(reviewForm.getUsername());
				review = bookieService.addReview(review, book, user);
				returnView = "redirect:/review/" + review.getId();
			} catch (Exception e) {
				returnView = "redirect:/error";
			}
		}
		return returnView;
	}
}


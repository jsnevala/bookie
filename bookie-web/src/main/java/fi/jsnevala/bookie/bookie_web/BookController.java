package fi.jsnevala.bookie.bookie_web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import fi.jsnevala.bookie.bookie_data.Book;
import fi.jsnevala.bookie.bookie_services.Bookieservice;
import fi.jsnevala.bookie.bookie_services.NotFoundException;
 
/**
 * 
 * Handles requests for book pages
 *
 */
@Controller
public class BookController {
 
	@Autowired
	Bookieservice bookieService;

	@RequestMapping(value="/book", method = RequestMethod.GET) 
	public ModelAndView allBooks() {
		List<Book> allBooks = bookieService.getAllBooks();
		ModelAndView mav = new ModelAndView("allBooks");
		mav.addObject(allBooks);
		return mav;
	}

	@RequestMapping(value="/book/{bookId}", method = RequestMethod.GET)
	public ModelAndView getSingleBook(@PathVariable Integer bookId) {
		ModelAndView mav = new ModelAndView();
		try {
			mav.setViewName("singleBook");
			Book book = bookieService.getBook(bookId);
			mav.addObject("book", book);			
		} catch (NotFoundException e) {
			mav.setViewName("error");
			mav.addObject("message", "Book with id " + bookId + " not found!");
		}
		return mav;
	}
}

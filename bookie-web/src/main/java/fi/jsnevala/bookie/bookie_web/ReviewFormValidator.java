package fi.jsnevala.bookie.bookie_web;

import org.springframework.validation.Errors;
import static org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace;

import fi.jsnevala.bookie.bookie_data.User;
import fi.jsnevala.bookie.bookie_services.Bookieservice;
import fi.jsnevala.bookie.bookie_services.NotFoundException;

/**
 * 
 * Validates add review form
 *
 */
public class ReviewFormValidator {
	
	public void validate(ReviewForm form, Bookieservice bookieService, Errors errors) {
		rejectIfEmptyOrWhitespace(errors, "book.title", "required", "required");
		rejectIfEmptyOrWhitespace(errors, "book.author", "required", "required");
		rejectIfEmptyOrWhitespace(errors, "book.year", "required", "required");
		rejectIfEmptyOrWhitespace(errors, "book.language", "required", "required");

		rejectIfEmptyOrWhitespace(errors, "username", "required", "required");
		rejectIfEmptyOrWhitespace(errors, "review.comments", "required", "required");
		rejectIfEmptyOrWhitespace(errors, "review.rating", "required", "required");

		if (!errors.hasFieldErrors("username")) {
			try {
				@SuppressWarnings(value = { "unused" })
				User user = bookieService.getUser(form.getUsername());
			} catch (NotFoundException e) {
				errors.rejectValue("username", "notExist", "Username does not exist.");				
			}
		}
	}	
}

<%@ tag description="Bookie page template" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@attribute name="sidebar" fragment="true" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Bookie</title>
<style type="text/css">
<%@include file="../../css/styles.css" %>
</style>
</head>

<body>
<div id="container">
	<div id="header">
    	<h1><a href="/">Bookie</a></h1>
        <h2>Books and stuff</h2>
        <div class="clear"></div>
    </div>
    <div id="nav">
    	<ul>
        	<li><a href="/user">Users</a></li>
            <li><a href="/review">Reviews</a></li>
            <li><a href="/book">Books</a></li>
            <li><a href="/about">About</a></li>
        </ul>
    </div>
    <div id="body">
		<div id="content">
	      <jsp:doBody/>
        </div>
        
        <div class="sidebar">
            <ul>	
               <li>
	                <h4><span>Actions</span></h4>
					<sec:authorize access="isAuthenticated()">
 		                <p><i>Logged in as <sec:authentication property="principal.username" /></i></p>
					</sec:authorize>
                    <ul class="blocklist">
						<sec:authorize access="isAnonymous()">
							<li><a href="/login">Login</a></li>
						</sec:authorize>
						<sec:authorize access="isAuthenticated()">
							<li><a href="/addreview">Add new review</a></li>
						</sec:authorize>
						<sec:authorize access="isAnonymous()">
	    					<li><a href="/adduser">Add new user</a></li>
						</sec:authorize>
						<li><a href="/user">List all users</a></li>
						<li><a href="/review">List all reviews</a></li>
						<li><a href="/book">List all books</a></li>	    
						<sec:authorize access="isAuthenticated()">
							<li><a href="/logout">Logout</a></li>	    
						</sec:authorize>
                    </ul>
                </li>
                
                <li>
                    <h4>About</h4>
                    <ul>
                        <li>
                        	<p style="margin: 0;">Bookie is created by Jussi Nevalainen. The source code can be found at <a href="https://bitbucket.org/jsnevala/bookie" target="_blank">BitBucket</a>. More info in <a href="/about">About</a> page.</p>
                        </li>
                    </ul>
                </li>
                
            </ul> 
        </div>
    	<div class="clear"></div>
    </div>
</div>
<div id="footer">
    <div class="footer-content">
    	<div class="footer-width">

            <span class="sitename">Bookie</span>
                <p class="footer-links">
                <a href="/user">Users</a> |
                <a href="/review">Reviews</a> |
                <a href="/book">Books</a> |
                <a href="/about">About</a> |
            </p>
         </div>
    </div>
    <div class="footer-width footer-bottom">
        <p>&copy; YourSite 2010. Design by <a href="http://www.spyka.net">Free CSS Templates</a> | <a href="http://www.justfreetemplates.com">Free Web Templates</a></p>
     </div>

</div>
</body>
</html>

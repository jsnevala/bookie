<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/functions' prefix='fn' %>
<%@ taglib uri='http://www.springframework.org/tags/form' prefix='f' %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ page session="true" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<t:basepage>
    <jsp:body>
        <h2>Add a new review</h2>
	
		<f:form method="post" modelAttribute="reviewForm" action="/addreview">
			<h4>Book</h4>
			<table>
				<tr>
					<td class="form"><f:label path="book.title">Title</f:label></td>
					<td class="form"><f:input path="book.title" /></td>
					<td class="form"><f:errors path="book.title" cssClass="errors"/></td>
				</tr>
				<tr>			
					<td class="form"><f:label path="book.author">Author</f:label></td>
					<td class="form"><f:input path="book.author" /></td>
					<td class="form"><f:errors path="book.author" cssClass="errors"/></td>
				</tr>
				<tr>			
					<td class="form"><f:label path="book.year">Publish year</f:label></td>
					<td class="form"><f:input path="book.year" /></td>
					<td class="form"><f:errors path="book.year" cssClass="errors"/></td>
				</tr>
				<tr>			
					<td class="form"><f:label path="book.language">Language</f:label></td>
					<td class="form"><f:input path="book.language" /></td>
					<td class="form"><f:errors path="book.language" cssClass="errors"/></td>
				</tr>
			</table>
			<h4>Review</h4>
			<table>
				<tr>			
					<td class="form"><f:label path="username">Reviewer</f:label></td>
					<td class="form"><f:input readonly="true" path="username" /></td>
					<td class="form"><f:errors path="username" cssClass="errors"/></td>
				</tr>
				<tr>
					<td class="form"><f:label path="review.rating">Rating</f:label></td>
					<td class="form"><f:select path="review.rating" items="${ratingScale}" /></td>
					<td class="form"><f:errors path="review.rating" cssClass="errors"/></td>
				</tr>
				<tr>			
					<td class="form"><f:label path="review.comments">Comments</f:label></td>
					<td class="form"><f:textarea path="review.comments" rows="5" cols="30" /></td>
					<td class="form"><f:errors path="review.comments" cssClass="errors"/></td>
				</tr>
				<tr>
					<td colspan="2" class="form">
						<input type="submit" value="Submit new review"/>
					</td>
				</tr>
			</table>
		</f:form>
    </jsp:body>
</t:basepage>

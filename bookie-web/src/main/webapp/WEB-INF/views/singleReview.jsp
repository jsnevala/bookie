<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ page session="true" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<% pageContext.setAttribute("newLineChar", "\n"); %>
<% pageContext.setAttribute("newLineHtml", "<br>"); %>

<t:basepage>
    <jsp:body>
		<h2>Review</h2>
		<table>
			<thead><tr>
			<th>Title</th>
			<th>Author</th>
			<th>Reviewer</th>
			<th>Reviewed on</th>
			<th>Rating</th>
			</tr></thead>
			<tr>
				<td><a href="/book/${review.book.id}" title="Click for book details"><c:out value="${review.book.title}"/></a></td>
				<td><c:out value="${review.book.author}"/></td>
				<td><a href="/user/${review.reviewer.username}" title="Click for user details"><c:out value="${review.reviewer.username}"/></a></td>
				<td><fmt:formatDate value="${review.created}" pattern="dd MMM yyyy"/></td>
				<td><c:out value="${review.rating}"/></td>
			</tr>
			<tr>
			<tr>
				<c:set var="comments" value="${fn:escapeXml(review.comments)}" scope="page" />
 				<td colspan="5">${fn:replace(comments, newLineChar, newLineHtml)}</td>  
			</tr>
		</table>
		<br><br>
		<div id="otherinfo">
			<h4>Other reviews for this book:</h4>
			<table>
				<thead><tr>
				<th>Reviewer</th>
				<th>Rating</th>
				<th>Comments</th>
				</tr></thead>
				<c:forEach var="otherreview" items="${review.book.reviews}">
					<c:choose>
					    <c:when test="${otherreview.reviewer.id != review.reviewer.id}">
							<tr>
								<td><a href="/user/${otherreview.reviewer.username}" title="Click for user details"><c:out value="${otherreview.reviewer.username}"/></a></td>
								<td><c:out value="${otherreview.rating}"/></td>
								<td><a href="/review/${otherreview.id}" title="Click for review details">View full review</a></td>
							</tr>
					    </c:when>
					</c:choose>				
				</c:forEach>
			</table>
		</div>
    </jsp:body>
</t:basepage>

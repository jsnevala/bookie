<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ page session="true" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<t:basepage>
    <jsp:body>
		<h2>Book info</h2>
		<table>
			<thead><tr>
			<th>Title</th>
			<th>Author</th>
			<th>Language</th>
			<th>Publish year</th>
			</tr></thead>
			<tr>
				<td><a href="/book/${book.id}" title="Click for book details"><c:out value="${book.title}"/></a></td>
				<td><c:out value="${book.author}"/></td>
				<td><c:out value="${book.language}"/></td>
				<td><c:out value="${book.year}"/></td>
			</tr>
		</table>
		<br><br>
		<div id="otherinfo">
			<h4>Reviews for this book</h4>
			<table>
				<thead><tr>
				<th>Reviewer</th>
				<th>Rating</th>
				<th>Comments</th>
				</tr></thead>
				<c:forEach var="review" items="${book.reviews}">
					<tr>
						<td><a href="/user/${review.reviewer.username}" title="Click for user details"><c:out value="${review.reviewer.username}"/></a></td>
						<td><c:out value="${review.rating}"/></td>
						<td><a href="/review/${review.id}" title="Click for review details">View full review</a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
    </jsp:body>
</t:basepage>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/functions' prefix='fn' %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ page session="true" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<t:basepage>
    <jsp:body>
        <h2>List of all users</h2>
		<table>
			<thead><tr>
			<th>Username</th>
			<th class=shrunk>Reviewed books</th>
			</tr></thead>
			<c:forEach var="user" items="${userList}">
			<tr>
				<td><a href="/user/${user.username}"><c:out value="${user.username}"/></a></td>
				<td><c:out value="${fn:length(user.reviews)}"/></td>
			</tr>
			</c:forEach>
		</table>
    </jsp:body>
</t:basepage>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ page session="true" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<t:basepage>

    <jsp:body>
        <h2>Welcome to Bookie!</h2>
		<br>
        <p>
	        Bookie is a service where users can add reviews for books in order to keep track of what they have read over the
	        years. It is also possible to discover new and interesting books by browsing reviews and books added by 
	        other users.
        </p>
		<sec:authorize access="isAnonymous()">
	        <p>
	        You may create a <a href="/adduser">new user</a> or <a href="/login">login</a> with username 'guest' and password 'guest' if you wish to add new reviews.
	        </p>
		</sec:authorize>
        
    </jsp:body>

</t:basepage>

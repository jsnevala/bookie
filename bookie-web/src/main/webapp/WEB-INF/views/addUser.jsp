<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/functions' prefix='fn' %>
<%@ taglib uri='http://www.springframework.org/tags/form' prefix='f' %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ page session="true" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<t:basepage>
    <jsp:body>
        <h2>Add a new user</h2>
	
		<f:form method="post" modelAttribute="user" action="/adduser">
			<table>
				<tr>
					<td class="form"><f:label path="username">Username</f:label></td>
					<td class="form"><f:input path="username" /> <f:errors path="username" cssClass="errors"/></td>
				</tr>
				<tr>			
					<td class="form"><f:label path="location">Location</f:label></td>
					<td class="form"><f:input path="location" /> <f:errors path="location" cssClass="errors"/></td>
				</tr>
				<tr>			
					<td class="form"><f:label path="password">Password</f:label></td>
					<td class="form"><f:input path="password" type="password" /> <f:errors path="password" cssClass="errors"/></td>
				</tr>
				<tr>
					<td colspan="2" class="form">
						<input type="submit" value="Create new user"/>
					</td>
				</tr>
			</table>
		</f:form>
    </jsp:body>
</t:basepage>

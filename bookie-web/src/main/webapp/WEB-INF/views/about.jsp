<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ page session="true" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<t:basepage>
    <jsp:body>
        <h2>About Bookie</h2>
        <br>
        <h3>Author</h3>
        <p>
        	Jussi Nevalainen
        </p>
        <h3>Development</h3>
        <p>
	        I created this service as a personal hobby project, with the intention of learning modern web 
	        application design and implementation using Java. 
	    </p><p>
	    	The project development followed 
	        roughly the outline described in book <a href="http://www.docendo.fi/tuote/951-0-34166-5" 
	        target="_blank">Tehokas Java EE-sovellustuotanto</a> by Janne Kuha. Since the book is rather old 
	        (published in 2008), many tools and frameworks have evolved since then and thus not all information in 
	        the book was relevant anymore. Good resources for up-to-date information are online reference guides 
	        (as well as JavaDoc) of used frameworks and various online forums (namely 
	        <a href="http://www.stackoverflow.com" target="_blank">Stack Overflow</a>) and blogs.
	        Despite the book being partly outdated, I found it great to start with as it explained the concepts
	        clearly without focusing too much on details like code syntax etc.
        </p>
		<h3>Tools and frameworks used in the project</h3>
		<ul>
			<li><a href="http://www.eclipse.org" target="_blank">Eclipse</a></li>
			<li><a href="http://maven.apache.org" target="_blank">Maven</a></li>
			<li><a href="http://www.hibernate.org" target="_blank">Hibernate</a></li>
			<li><a href="http://www.springsource.org" target="_blank">Spring Framework</a></li>
			<li><a href="http://hsqldb.org/" target="_blank">HyperSQL</a> (testing) / <a href="http://www.mysql.com" target="_blank">MySQL</a> (production)</li>
			<li><a href="http://www.git-scm.com" target="_blank">Git</a></li>
			<li><a href="http://www.jboss.org/jbossas" target="_blank">JBoss Application Server</a></li>
			<li><a href="http://www.junit.org" target="_blank">JUnit</a></li>
			<li><a href="http://www.easymock.org" target="_blank">EasyMock</a></li>
		</ul>
		<h3>Implementation</h3>
        <p> 
			The source code and all relevant project files can be found at <a href="https://bitbucket.org/jsnevala/bookie" target="_blank">BitBucket</a>. The application is hosted in RedHat's <a href="http://openshift.redhat.com" target="_blank">OpenShift</a>.
        </p>
        <h3>Contact</h3>
        <p>
        	Author's first name dot last name at iki dot fi. See also <a href="http://www.linkedin.com/pub/jussi-nevalainen/4/148/a67" target="_blank">LinkedIn</a> profile.
        </p>
    </jsp:body>

</t:basepage>

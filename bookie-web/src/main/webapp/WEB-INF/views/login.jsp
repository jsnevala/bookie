<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ page session="true" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<t:basepage>
    <jsp:body>
        <h2>Login</h2>
		<c:if test="${not empty error}">
			<div class="errors">
				Login failed, please try again.<br /> 
				Reason: ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
			</div>
		</c:if>

		<c:if test="${not empty message}">
			<div>
				${message}
			</div>
		</c:if>

		<form method="post" action="<c:url value='j_spring_security_check' />" >
 
			<table>
				<tr>
					<td class="form">Username</td>
					<td class="form"><input type="text" name='j_username' /></td>
				</tr>
				<tr>			
					<td class="form">Password</td>
					<td class="form"><input type="password" name='j_password' /></td>
				</tr>
				<tr>
					<td colspan="2" class="form">
						<input type="submit" value="Login"/>
					</td>
				</tr>
			</table>
		</form>
    </jsp:body>
</t:basepage>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ page session="true" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<t:basepage>
    <jsp:body>
		<h2>User info</h2>
		<table>
			<thead><tr>
			<th>Username</th>
			<th>Location</th>
			<th>Join date</th>
			</tr></thead>
			<tr>
				<td><a href="/user/${user.username}"><c:out value="${user.username}"/></a></td>
				<td><c:out value="${user.location}"/></td>
				<td><fmt:formatDate value="${user.joinDate}" pattern="dd MMM yyyy"/></td>
			</tr>
		</table>
		<br><br>		
		<div id="otherinfo">
			<h4>Reviewed books</h4>
			<table>
				<thead><tr>
				<th>Title</th>
				<th>Author</th>
				<th>Reviewed on</th>
				<th>Rating</th>
				<th>Comments</th>
				</tr></thead>
				<c:forEach var="review" items="${user.reviews}">
					<tr>
						<td><a href="/book/${review.book.id}" title="Click for book details"><c:out value="${review.book.title}"/></a></td>
						<td><c:out value="${review.book.author}"/></td>
						<td><fmt:formatDate value="${review.created}" pattern="yyyy"/></td>
						<td><c:out value="${review.rating}"/></td>
						<td><a href="/review/${review.id}" title="Click for review details">View full review</a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
    </jsp:body>
</t:basepage>

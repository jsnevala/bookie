<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/functions' prefix='fn' %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ page session="true" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<t:basepage>
    <jsp:body>
        <h2>List of all books</h2>
		<table>
			<thead><tr>
			<th>Title</th>
			<th>Author</th>
			<th>Publish year</th>
			<th>Language</th>
			</tr></thead>
			<c:forEach var="book" items="${bookList}">
			<tr>
				<td><a href="/book/${book.id}"><c:out value="${book.title}" /></a></td>
				<td><c:out value="${book.author}" /></td>
				<td><c:out value="${book.year}" /></td>
				<td><c:out value="${book.language}" /></td>
			</tr>
			</c:forEach>
		</table>
    </jsp:body>
</t:basepage>

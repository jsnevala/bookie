<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/functions' prefix='fn' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ page session="true" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<t:basepage>
    <jsp:body>
        <h2>List of all reviews</h2>
		<table>
			<thead><tr>
			<th>Book title</th>
			<th>Book author</th>
			<th>Reviewer</th>
			<th>Reviewed on</th>
			<th>Rating</th>
			</tr></thead>
			<c:forEach var="review" items="${reviewList}">
			<tr>
				<td class="reviewinfo"><a href="/book/${review.book.id}" title="Click for book details"><c:out value="${review.book.title}"/></a></td>
				<td class="reviewinfo"><c:out value="${review.book.author}"/></td>
				<td class="reviewinfo"><a href="/user/${review.reviewer.username}" title="Click for user details"><c:out value="${review.reviewer.username}"/></a></td>
				<td class="reviewinfo"><fmt:formatDate value="${review.created}" pattern="MMM yyyy"/></td>
				<td class="reviewinfo"><c:out value="${review.rating}"/></td>
			</tr>
			<tr>
				<td colspan="5" class="reviewmoreinfo"><a href="/review/${review.id}" title="Click for review details">View full review</a></td>
			</tr>
			</c:forEach>
		</table>
    </jsp:body>
</t:basepage>

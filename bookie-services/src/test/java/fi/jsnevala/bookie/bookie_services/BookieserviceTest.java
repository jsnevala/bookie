package fi.jsnevala.bookie.bookie_services;

import static org.easymock.EasyMock.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import fi.jsnevala.bookie.bookie_data.Book;
import fi.jsnevala.bookie.bookie_data.BookDao;
import fi.jsnevala.bookie.bookie_data.ReviewDao;
import fi.jsnevala.bookie.bookie_data.Review;
import fi.jsnevala.bookie.bookie_data.User;
import fi.jsnevala.bookie.bookie_data.UserDao;

import static org.junit.Assert.assertEquals;

public class BookieserviceTest {
	protected static Logger logger = Logger.getLogger(BookieserviceTest.class);
	
	public void logTestHeader(String text) {
		logger.info("##### Test: " + text + " #####");		
	}
	
	@Before
	public void setLoggerLevel() {
		logger.setLevel(Level.DEBUG);
	}
	
	@Test
	public void testAddUser() throws Exception {
		logTestHeader("Add user");

		User user = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");

		UserDao userDao = createMock(UserDao.class);		
		userDao.save(user);

		BookieserviceImplementation service = new BookieserviceImplementation();
		service.setUserDao(userDao);

		replay(userDao);
		
		service.addUser(user);
		
		verify(userDao);
	}

	@Test
	public void testAddAndGetUser() throws Exception {
		logTestHeader("Add and get user");

		User user1 = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		User user2 = new User("stanley", "NYC", new Date(), "dummypassword");
		User user3 = new User("mike", "SF", new Date(), "dummypassword");

		UserDao userDao = createMock(UserDao.class);		
		userDao.save(user1);
		userDao.save(user2);
		userDao.save(user3);

		BookieserviceImplementation service = new BookieserviceImplementation();
		service.setUserDao(userDao);

		List<User> users = new ArrayList<User>();
		users.add(user1);
		users.add(user2);
		users.add(user3);
		expect(userDao.fetchAll()).andReturn(users);

		replay(userDao);
		
		service.addUser(user1);
		service.addUser(user2);
		service.addUser(user3);

		User testUser = service.getUser("jussi2");
		assertEquals("Wrong user returned (" + testUser.getUsername() + ")", testUser, user1);		
		verify(userDao);
	}

	@Test
	public void testNoReviewsExist() {

		logTestHeader("No reviews exist");
		ReviewDao reviewDao = createMock(ReviewDao.class);		
		BookieserviceImplementation service = new BookieserviceImplementation();
		
		service.setReviewDao(reviewDao);
		
		expect(reviewDao.fetchAll()).andReturn(new ArrayList<Review>());
		replay(reviewDao);
		
		List<Review> allReviews = service.getAllReviews();
		
		assertEquals(allReviews.size() + " reviews found (expected 0).", 0, allReviews.size());		
		
		verify(reviewDao);
	}

	@Test
	public void testOneReviewExists() {

		logTestHeader("One review exists");
		ReviewDao reviewDao = createMock(ReviewDao.class);		

		BookieserviceImplementation service = new BookieserviceImplementation();
		service.setReviewDao(reviewDao);
		
		List<Review> refReviews = new ArrayList<Review>();
		refReviews.add(new Review("ok", 4.0, new Date()));
		expect(reviewDao.fetchAll()).andReturn(refReviews);
		replay(reviewDao);
		
		List<Review> allReviews = service.getAllReviews();
		
		assertEquals("Reviews size != 1", 1, allReviews.size());
		verify(reviewDao);
	}

	@Test
	public void testOneBookExists() {

		logTestHeader("One book exists");
		BookDao bookDao = createMock(BookDao.class);		

		BookieserviceImplementation service = new BookieserviceImplementation();
		service.setBookDao(bookDao);
		
		List<Book> refBooks = new ArrayList<Book>();
		refBooks.add(new Book("ABCD", "Me, myself and I", 1800, "Finnish"));
		expect(bookDao.fetchAll()).andReturn(refBooks);
		replay(bookDao);
		
		List<Book> allBooks = service.getAllBooks();
		
		assertEquals("Books size != 1", 1, allBooks.size());
		verify(bookDao);
	}

	@Test
	public void testAddReview() {

		logTestHeader("Add one review");

		UserDao userDao = createMock(UserDao.class);		
		BookDao bookDao = createMock(BookDao.class);		
		ReviewDao reviewDao = createMock(ReviewDao.class);		
		Book book = createMock(Book.class);
		User user = createMock(User.class);
		Review review = createMock(Review.class);
		userDao.save(user);
		book.addReview(review);
		user.addReview(review);
				
		BookieserviceImplementation service = new BookieserviceImplementation();
		service.setUserDao(userDao);
		service.setBookDao(bookDao);
		service.setReviewDao(reviewDao);

		expect(bookDao.fetchAll()).andReturn(new ArrayList<Book>());
		expect(bookDao.fetchAll()).andReturn(new ArrayList<Book>());
		expect(reviewDao.fetchAll()).andReturn(new ArrayList<Review>());

		replay(userDao);
		replay(book);
		replay(user);
		replay(review);
			
		service.addReview(review, book, user);
		
		verify(userDao);
		verify(book);
		verify(user);
		verify(review);
	}
}

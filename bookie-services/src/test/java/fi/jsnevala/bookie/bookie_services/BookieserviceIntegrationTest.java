package fi.jsnevala.bookie.bookie_services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import fi.jsnevala.bookie.bookie_data.Book;
import fi.jsnevala.bookie.bookie_data.Review;
import fi.jsnevala.bookie.bookie_data.User;

@ContextConfiguration(locations = { "classpath:spring-integrationtests.xml" })
public class BookieserviceIntegrationTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private Bookieservice bookieService;
	
	@Autowired
	EntityManagerFactory entityManagerFactory;

	EntityManager entityManager;
	
	public void logTestHeader(String text) {
		logger.info("##### Test: " + text + " #####");		
	}

	@Before
	public void testSetUp() {
		entityManager = entityManagerFactory.createEntityManager();
	}

	@Test
	public void testIntMultipleReviewsExist() {
		logTestHeader("Multiple reviews");

		List<Review> allReviews = bookieService.getAllReviews();
		
		assertEquals(allReviews.size() + " reviews found (expected 12).", 12, allReviews.size());		
	}

	@Test
	public void testIntAddOneUserWithNoReviews() throws Exception {
		logTestHeader("Add one user with no reviews");
	
		User user = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");

		bookieService.addUser(user);
		
		User testUser = bookieService.getUser("jussi2");
		
		assertEquals(testUser.getReviews().size() + " reviews found (expected 0).", 0, testUser.getReviews().size());		
	}

	@Test
	public void testIntAddOneUserThenAddReview() throws Exception {
		logTestHeader("Add one user and then add review");
				
		User user = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		bookieService.addUser(user);

		Book book = new Book("0011223344556", "What technology wants", "Kelly, Kevin", 2010, "English");
		Review review = new Review("Crap", 0.0, new Date());

		user = bookieService.getUser("jussi2");
		
		bookieService.addReview(review, book, user);
		
		Set<Review> reviews = bookieService.getUser("jussi2").getReviews();
		
		assertEquals(reviews.size() + " reviews found (expected 1).", 1, reviews.size());		
	}


	@Test
	public void testIntTwoUsersTwoReviewsForSameBook() throws Exception {
		User user1 = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		bookieService.addUser(user1);

		User user2 = new User("stanley", "NYC", new Date(), "dummypassword");
		bookieService.addUser(user2);

		Book book = new Book("0011223344556", "What technology wants", "Kelly, Kevin", 2010, "English");

		Review reviewUser1 = new Review("Crap", 0.0, new Date());
		Review reviewUser2 = new Review("Yeah baby", 4.0, new Date());
		
		bookieService.addReview(reviewUser1, book, user1);
		bookieService.addReview(reviewUser2, book, user2);

		Set<Review> reviewsUser1 = bookieService.getUser("jussi2").getReviews();
		Set<Review> reviewsUser2 = bookieService.getUser("stanley").getReviews();

		Iterator<Review> itr1 = reviewsUser1.iterator();
		Iterator<Review> itr2 = reviewsUser2.iterator();
		
		Review testReviewUser1 = itr1.next();
		Review testReviewUser2 = itr2.next();
		
		assertEquals("Books mismatch.", testReviewUser1.getBook(), testReviewUser2.getBook());		
	}

	@Test
	public void testIntTwoUsersTwoReviewsForSameBookWithWhitespace() throws Exception {
		User user1 = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		bookieService.addUser(user1);

		User user2 = new User("stanley", "NYC", new Date(), "dummypassword");
		bookieService.addUser(user2);

		Book book1 = new Book("What technology wants ", "Kelly, Kevin", 2010, "English");
		Book book2 = new Book("What technology wants", "Kelly, Kevin", 2010, "English");

		Review reviewUser1 = new Review("Crap", 0.0, new Date());
		Review reviewUser2 = new Review("Yeah baby", 4.0, new Date());
		
		bookieService.addReview(reviewUser1, book1, user1);
		bookieService.addReview(reviewUser2, book2, user2);

		Set<Review> reviewsUser1 = bookieService.getUser("jussi2").getReviews();
		Set<Review> reviewsUser2 = bookieService.getUser("stanley").getReviews();

		Iterator<Review> itr1 = reviewsUser1.iterator();
		Iterator<Review> itr2 = reviewsUser2.iterator();
		
		Review testReviewUser1 = itr1.next();
		Review testReviewUser2 = itr2.next();

		assertEquals("Books mismatch.", testReviewUser1.getBook(), testReviewUser2.getBook());		
	}

	@Test
	public void testIntTwoUsersTwoReviewsForSameBookDifferentLanguage() throws Exception {
		User user1 = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		bookieService.addUser(user1);

		User user2 = new User("stanley", "NYC", new Date(), "dummypassword");
		bookieService.addUser(user2);

		Book book1 = new Book("What technology wants", "Kelly, Kevin", 2010, "English");
		Book book2 = new Book("What technology wants", "Kelly, Kevin", 2010, "Finnish");

		Review reviewUser1 = new Review("Crap", 0.0, new Date());
		Review reviewUser2 = new Review("Yeah baby", 4.0, new Date());
		
		bookieService.addReview(reviewUser1, book1, user1);
		bookieService.addReview(reviewUser2, book2, user2);

		Set<Review> reviewsUser1 = bookieService.getUser("jussi2").getReviews();
		Set<Review> reviewsUser2 = bookieService.getUser("stanley").getReviews();

		Iterator<Review> itr1 = reviewsUser1.iterator();
		Iterator<Review> itr2 = reviewsUser2.iterator();
		
		Review testReviewUser1 = itr1.next();
		Review testReviewUser2 = itr2.next();
		
		assertNotSame("Books match.", testReviewUser1.getBook(), testReviewUser2.getBook());		
	}

	@Test(expected = UsernameAlreadyExistsException.class)
	public void testIntUserNameAlreadyExists() throws Exception {
		User user1 = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		bookieService.addUser(user1);

		User user2 = new User("jussi2", "NYC", new Date(), "dummypassword");
		bookieService.addUser(user2);
	}
}

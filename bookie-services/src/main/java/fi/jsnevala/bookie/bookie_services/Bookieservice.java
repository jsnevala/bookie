package fi.jsnevala.bookie.bookie_services;

import java.util.List;

import fi.jsnevala.bookie.bookie_data.Book;
import fi.jsnevala.bookie.bookie_data.Review;
import fi.jsnevala.bookie.bookie_data.User;

public interface Bookieservice {

	/**
	 * Adds a new user to Bookie
	 * @param user User to be added
	 * @throws Exception
	 */
	public void addUser(User user) throws Exception;
	
	/**
	 * Adds a new review to Bookie
	 * @param review Review to be added
	 * @param book Reviewed book
	 * @param user Reviewer
	 * @return Review that has been added (managed in persistence context)
	 */
	public Review addReview(Review review, Book book, User user);
	
	/**
	 * Returns all reviews
	 * @return List of all reviews
	 */
	public List<Review> getAllReviews();

	/**
	 * Returns all users
	 * @return List of all users
	 */
	public List<User> getAllUsers();
	
	/**
	 * Returns all books
	 * @return List of all books
	 */
	public List<Book> getAllBooks();

	/**
	 * Returns a single user
	 * @param queriedUsername Requested username 
	 * @return User with requested username
	 * @throws NotFoundException
	 */
	public User getUser(String queriedUserName) throws NotFoundException;

	/**
	 * Returns a review with requested id
	 * @param reviewId Id of the review
	 * @return Review with requested id
	 * @throws NotFoundException
	 */
	public Review getReview(Integer reviewId) throws NotFoundException;

	/**
	 * Returns a book with requested id
	 * @param bookId Id of the book
	 * @return Book with requested id
	 * @throws NotFoundException
	 */
	public Book getBook(Integer bookId) throws NotFoundException;
}

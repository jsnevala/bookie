package fi.jsnevala.bookie.bookie_services;

/**
 * This exception is thrown when user already exists in the system
 *
 */
public class UsernameAlreadyExistsException extends RuntimeException {
    /**
	 * Version for serialize 
	 */
	private static final long serialVersionUID = -8496017603073951392L;

	/**
	 * Construct new UsernameAlreadyExistsException
	 */
	public UsernameAlreadyExistsException() {
        super();
    }

}

package fi.jsnevala.bookie.bookie_services;

/**
 * This exception is thrown when requested object is not found in the system
 *
 */
public class NotFoundException extends Exception {
	/**
	 * Version for serialize
	 */
	private static final long serialVersionUID = -4171947626307129159L;

	/**
	 * Construct new NotFoundException
	 */
	public NotFoundException() {
        super();
    }

}

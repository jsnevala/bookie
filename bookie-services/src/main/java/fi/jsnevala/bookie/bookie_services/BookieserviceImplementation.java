package fi.jsnevala.bookie.bookie_services;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;


import fi.jsnevala.bookie.bookie_data.Book;
import fi.jsnevala.bookie.bookie_data.BookDao;
import fi.jsnevala.bookie.bookie_data.Review;
import fi.jsnevala.bookie.bookie_data.ReviewDao;
import fi.jsnevala.bookie.bookie_data.User;
import fi.jsnevala.bookie.bookie_data.UserDao;

import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * Implementation of Bookie service layer
 *
 */
@Transactional
public class BookieserviceImplementation implements Bookieservice {

	/**
	 * Logging
	 */
	protected static Logger logger = Logger.getLogger(BookieserviceImplementation.class);

	/**
	 * User DAO
	 */
	private UserDao userDao;

	/**
	 * Review DAO
	 */
	private ReviewDao reviewDao;

	/**
	 * Book DAO
	 */
	private BookDao bookDao;
	
	/**
	 * Construct new Bookie service layer
	 */
	public BookieserviceImplementation() {
	}
	
	/**
	 * Set user DAO
	 * @param userDao User DAO to be used
	 */
	public void setUserDao(final UserDao userDao) {
		this.userDao = userDao;
	}

	/**
	 * Set review DAO
	 * @param reviewDao Review DAO to be used
	 */
	public void setReviewDao(final ReviewDao reviewDao) {
		this.reviewDao = reviewDao;
	}

	/**
	 * Set book DAO
	 * @param bookDao Book DAO to be used
	 */
	public void setBookDao(final BookDao bookDao) {
		this.bookDao = bookDao;
	}

	/**
	 * Adds a new user to Bookie
	 * @param user User to be added
	 * @throws Exception
	 */
	public void addUser(User user) throws Exception {
		try {
			userDao.save(user);			
		} catch (Exception e) {
			ConstraintViolationException cve = null;
			if (e.getClass().equals(ConstraintViolationException.class)) {
				cve = (ConstraintViolationException) e;
			} else if (e.getCause().getClass().equals(ConstraintViolationException.class)) {
				cve = (ConstraintViolationException) e.getCause();
			}
			if ((cve != null) && (isUniqueConstraintViolation(cve.getSQLState()))) {
				throw new UsernameAlreadyExistsException();					
			} else {
				throw e;
			}
		}
	}

	/**
	 * Checks if book is already stored in DB
	 * 
	 * @return Id of stored book if already stored in DB, null otherwise
	 */
	private Integer getStoredBookId(Book book) {
		List<Book> allBooks = getAllBooks();
		Integer storedBookId = null;
		
		if (allBooks != null) {
			Iterator<Book> itr = allBooks.iterator(); 
			
			while(itr.hasNext() && (storedBookId == null)) {
				Book storedBook = itr.next(); 
				if (book.getTitle().equals(storedBook.getTitle()) &&
					book.getAuthor().equals(storedBook.getAuthor()) &&
					book.getYear().equals(storedBook.getYear()) &&
					book.getLanguage().equals(storedBook.getLanguage())) {
					storedBookId = storedBook.getId();					
				}
			}
		}
		return storedBookId;
	}

	/**
	 * Checks if review is already stored in DB
	 * 
	 * @return Id of stored review if already stored in DB, null otherwise
	 */
	private Integer getStoredReviewId(Review review) {
		List<Review> allReviews = getAllReviews();
		Integer storedReviewId = null;
		
		if (allReviews != null) {
			Iterator<Review> itr = allReviews.iterator(); 
			
			while(itr.hasNext() && (storedReviewId == null)) {
				Review storedReview = itr.next(); 
				if (review.getBook().getTitle().equals(storedReview.getBook().getTitle()) &&
					review.getComments().equals(storedReview.getComments()) &&
					review.getRating().equals(storedReview.getRating()) &&
					review.getReviewer().getUsername().equals(storedReview.getReviewer().getUsername())) {
					storedReviewId = storedReview.getId();
				}
			}
		}
		return storedReviewId;
	}

	/**
	 * Adds a new review to Bookie
	 * @param review Review to be added
	 * @param book Reviewed book
	 * @param user Reviewer
	 * @return Review that has been added (managed in persistence context)
	 */
	public Review addReview(Review review, Book book, User user) {
		Integer storedBookId = getStoredBookId(book);
		Book storedBook = book;
		if (storedBookId != null) {
			try {
				storedBook = getBook(storedBookId);
			} catch (NotFoundException e) {
				e.printStackTrace(); 
			}
		}

		storedBook.addReview(review);
		user.addReview(review);
		userDao.save(user);
		
		Review managedReview = null;
		Integer reviewId = getStoredReviewId(review);
		if (reviewId != null) {
			managedReview = reviewDao.fetchWithPrimaryKey(reviewId); 			
		}
		return managedReview;
	}

	/**
	 * Returns all reviews
	 * @return List of all reviews
	 */
	public List<Review> getAllReviews() {
		List<Review> reviews = reviewDao.fetchAll();
		if (reviews != null) {
			Collections.sort(reviews, Review.ReviewComparator);
		}
		return reviews;
	}

	/**
	 * Returns all books
	 * @return List of all books
	 */
	public List<Book> getAllBooks() {
		List<Book> books = bookDao.fetchAll();
		if (books != null) {
			Collections.sort(books, Book.BookComparator);
		}
		return books;
	}

	/**
	 * Returns all users
	 * @return List of all users
	 */
	public List<User> getAllUsers() {
		List<User> users = userDao.fetchAll();
		if (users != null) {
			Collections.sort(users, User.UserComparator);
		}
		return users;
	}

	/**
	 * Returns a single user
	 * @param queriedUsername Requested username 
	 * @return User with requested username
	 * @throws NotFoundException
	 */
	public User getUser(String queriedUsername) throws NotFoundException {

		List<User> allUsers = getAllUsers();

		Iterator<User> itrUsers = allUsers.iterator(); 
		
		User queriedUser = null;
		while(itrUsers.hasNext()) {
			User user = itrUsers.next(); 
			if (user.getUsername().equals(queriedUsername)) {
				queriedUser = user;
				break;
			}
		}
		
		if (queriedUser == null) {
			throw new NotFoundException();
		}
		return queriedUser;
	}

	/**
	 * Returns a review with requested id
	 * @param reviewId Id of the review
	 * @return Review with requested id
	 * @throws NotFoundException
	 */
	public Review getReview(Integer reviewId) throws NotFoundException {

		List<Review> allReviews = getAllReviews();

		Iterator<Review> itr = allReviews.iterator(); 
		
		Review queriedReview = null;
		while(itr.hasNext()) {
			Review review = itr.next(); 
			if (review.getId().equals(reviewId)) {
				queriedReview = review;
				break;
			}
		}
		
		if (queriedReview == null) {
			throw new NotFoundException();
		}
		return queriedReview;
	}

	/**
	 * Returns a book with requested id
	 * @param bookId Id of the book
	 * @return Book with requested id
	 * @throws NotFoundException
	 */
	public Book getBook(Integer bookId) throws NotFoundException {

		List<Book> allBooks = getAllBooks();

		Iterator<Book> itr = allBooks.iterator(); 
		
		Book queriedBook = null;
		while(itr.hasNext()) {
			Book book = itr.next(); 
			if (book.getId().equals(bookId)) {
				queriedBook = book;
				break;
			}
		}
		
		if (queriedBook == null) {
			throw new NotFoundException();
		}
		return queriedBook;
	}

	/**
	 * Validates unique constraint violation during persist operation
	 * @param sqlState SQLState error code
	 * @return True if unique constraint violation, false otherwise
	 */
	private boolean isUniqueConstraintViolation(String sqlState) {
		// TODO find a defined value for sql state 23505
		if (sqlState.equals("23505")) {
			return true;
		} else {
			return false;
		}
		
	}
}

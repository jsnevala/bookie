package fi.jsnevala.bookie.bookie_data.test;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import org.junit.Before;
import org.junit.After;

import fi.jsnevala.bookie.bookie_data.Book;
import fi.jsnevala.bookie.bookie_data.Review;
import fi.jsnevala.bookie.bookie_data.User;



/**
 * Superclass for Bookie testcases
 */
public abstract class DatabaseTestCase {

	protected static Logger logger = Logger.getLogger(DatabaseTestCase.class);
	protected EntityManagerFactory entityManagerFactory;
	protected EntityManager entityManager;
	

	/**
	 * Test set-up
	 */
	@Before
	public void testSetUp() {
		logger.setLevel(Level.DEBUG);
		createEntityManager();
	}

	private void createEntityManager() {
		logger.info("Opening database connection.");
		entityManagerFactory = Persistence.createEntityManagerFactory("bookiePersistence");		
		entityManager = entityManagerFactory.createEntityManager();
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
		
	/**
	 * Test teardown
	 */
	@After
	public void closeEntityManager() {
		logger.info("Closing database connection.");
		entityManager.close();
		entityManagerFactory.close();
	}
	
	public void logTestHeader(String text) {
		logger.info("##### Test: " + text + " #####");		
	}

	/**
	 * Creates a new review
	 */
	public Review createReview(Book book, User user, String comments, Double rating) {
		Review review = createReviewWithDate(book, user, comments, rating, new Date());
		return review;
	}

	/**
	 * Creates a new review with specific date
	 */
	public Review createReviewWithDate(Book book, User user, String comments, Double rating, Date created) {
		Review review = new Review(comments, rating, created);
		book.addReview(review);
		user.addReview(review);
		return review;
	}

}

package fi.jsnevala.bookie.bookie_data.test;

import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityTransaction;

import static org.junit.Assert.*;
import org.junit.Test;

import fi.jsnevala.bookie.bookie_data.User;
import fi.jsnevala.bookie.bookie_data.Review;
import fi.jsnevala.bookie.bookie_data.Book;

public class EntityTest extends DatabaseTestCase {

	/**
	 * Store new item to database
	 */
	private <T> void persistEntity(T entity) {

		Pattern regex = Pattern.compile("(\\w+$)");
		Matcher matcher = regex.matcher(entity.getClass().toString());
		
		if (matcher.find()) {
			logger.info("Persisting " + matcher.group());
		} else {
			logger.info("Persisting " + entity.getClass().toString());
		}
		
		EntityTransaction tx = super.entityManager.getTransaction();
		tx.begin();
		super.entityManager.persist(entity);
		tx.commit();		
	}

	/**
	 * This method tests storing user data to database
	 */
	@Test
	public void testUserStoring() {
		logTestHeader("Creating new user");
		User user = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		
		persistEntity(user);

		Integer primaryKey = user.getId();
		EntityTransaction tx = super.entityManager.getTransaction();
		
		logger.info("Reading user from database");
		tx = super.entityManager.getTransaction();
		tx.begin();
		user = super.entityManager.find(User.class, primaryKey);
		tx.commit();
		
		assertEquals("Username was incorrect in database", "jussi2", user.getUsername());
	}

	/**
	 * This method tests adding a read book that is null to user
	 */
	@Test(expected = NullPointerException.class)
	public void testAddNullReviewToUser() {
		logTestHeader("Add null review to user");

		User user = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		user.addReview(null);
	}
	
	/**
	 * This method tests adding a review with 0.5 rating
	 */
	@Test
	public void testReviewPoint5Rating() {
		logTestHeader("Add a review with 0.5 rating");
		User user = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		Book book = new Book("1234567890123", "1984", "Orwell", 1900, "English");
		Review review = createReview(book, user, "Oh well", 0.5);
		persistEntity(user);
		Integer userPK = user.getId();

		logger.info("Reading user from database");
		EntityTransaction tx = super.entityManager.getTransaction();
		tx.begin();
		user = super.entityManager.find(User.class, userPK);
		tx.commit();

		assertTrue("Review does not exist in database", user.getReviews().contains(review));
	}		

	/**
	 * This method tests adding a review with 2.5 rating
	 */
	@Test
	public void testReviewTwoAndHalfRating() {
		logTestHeader("Add a review with 2.5 rating");
		User user = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		Book book = new Book("1234567890123", "1984", "Orwell", 1900, "English");
		Review review = createReview(book, user, "Oh well", 2.5);
		persistEntity(user);
		Integer userPK = user.getId();

		logger.info("Reading user from database");
		EntityTransaction tx = super.entityManager.getTransaction();
		tx.begin();
		user = super.entityManager.find(User.class, userPK);
		tx.commit();

		assertTrue("Review does not exist in database", user.getReviews().contains(review));
	}		

	/**
	 * This method tests adding a review with 0.6 rating
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testReviewPoint6Rating() {
		logTestHeader("Add a review with 0.6 rating");
		User user = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		Book book = new Book("1234567890123", "1984", "Orwell", 1900, "English");
		createReview(book, user, "Oh well", 0.6);
		persistEntity(user);
	}		

	/**
	 * This method tests adding a review with negative rating
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testReviewNegativeRating() {
		logTestHeader("Add a review with negative rating");
		User user = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		Book book = new Book("1234567890123", "1984", "Orwell", 1900, "English");
		createReview(book, user, "Oh well", -1.0);
		persistEntity(user);
	}		

	/**
	 * This method tests adding a review with rating 6.0
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testReviewSixRating() {
		logTestHeader("Add a review with 6.0 rating");
		User user = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		Book book = new Book("1234567890123", "1984", "Orwell", 1900, "English");
		createReview(book, user, "Oh well", 6.0);
		persistEntity(user);
	}		

	/**
	 * This method tests adding several reviews for a single user
	 */
	@Test
	public void testAddAndRemoveReviewsToUser() {
		logTestHeader("Add and remove reviews to user");

		User user = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		Book book1 = new Book("1234567890123", "1984", "Orwell", 1900, "English");
		Book book2 = new Book("0011223344556", "What technology wants", "Kelly, Kevin", 2010, "English");
		Book book3 = new Book("9911882277336", "The big book", "Guess", 0, "Hebrew");

		Review review1 = createReview(book1, user, "Awesome!", 5.0);
		Review review2 = createReview(book2, user, "Dirt!", 0.0);
		Review review3 = createReview(book3, user, "Mediocre!", 3.0);

		persistEntity(user);

		Integer userPK = user.getId();

		logger.info("Reading user from database");
		EntityTransaction tx = super.entityManager.getTransaction();
		tx.begin();
		user = super.entityManager.find(User.class, userPK);
		tx.commit();

		assertTrue("Review1 does not exist in database", user.getReviews().contains(review1));
		assertTrue("Review2 does not exist in database", user.getReviews().contains(review2));
		assertTrue("Review3 does not exist in database", user.getReviews().contains(review3));

	}		

	/**
	 * This method tests adding reviews from several users for a single book 
	 */
	@Test
	public void testAddManyReviewsForBook() {
		logTestHeader("Add many reviews for a single book");

		User user1 = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		User user2 = new User("stanley", "NYC", new Date(), "dummypassword");

		Book book1 = new Book("1234567890123", "1984", "Orwell", 1900, "English");
		Book book2 = new Book("0011223344556", "What technology wants", "Kelly, Kevin", 2010, "English");
		Book book3 = new Book("9911882277336", "The big book", "Guess", 0, "Hebrew");

		createReview(book1, user1, "Awesome!", 5.0);
		createReview(book2, user1, "Dirt!", 0.0);
		createReview(book3, user1, "Mediocre!", 3.0);

		persistEntity(user1);

		createReview(book1, user2, "Alright", 2.5);
		createReview(book3, user2, "Wouldn't recommend", 1.0);

		persistEntity(user2);

		Integer user1PK = user1.getId();
		Integer user2PK = user2.getId();

		logger.info("Reading users from database");
		EntityTransaction tx = super.entityManager.getTransaction();
		tx.begin();
		user1 = super.entityManager.find(User.class, user1PK);
		user2 = super.entityManager.find(User.class, user2PK);
		tx.commit();

		Set<Review> user1Reviews = user1.getReviews();
		Set<Review> user2Reviews = user2.getReviews();
		Iterator<Review> itrUser2 = user2Reviews.iterator(); 
		
		while(itrUser2.hasNext()) {
			Book user2Book = itrUser2.next().getBook();

			boolean isBookFound = false;
			Iterator<Review> itrUser1 = user1Reviews.iterator(); 
			while(itrUser1.hasNext() && !(isBookFound)) {
				Book user1Book = itrUser1.next().getBook();
				if (user1Book.equals(user2Book)) {
					isBookFound = true;
				}
			}
			assertTrue("Single book persisted several times.", isBookFound);
		}
	}		

}

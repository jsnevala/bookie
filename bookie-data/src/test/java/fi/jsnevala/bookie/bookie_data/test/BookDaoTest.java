package fi.jsnevala.bookie.bookie_data.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityTransaction;

import org.junit.Test;

import fi.jsnevala.bookie.bookie_data.Book;
import fi.jsnevala.bookie.bookie_data.User;

public class BookDaoTest extends CommonDaoTestCase {
	@Test
	public void testFetchAllBooksReviewedByUser() throws Exception {
		EntityTransaction tx = getEntityManager().getTransaction();
		tx.begin();
		
		logTestHeader("Fetch all books that are reviewed by a user");
		User user1 = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		User user2 = new User("stanley", "NYC", new Date(), "dummypassword");

		Book book1 = new Book("1234567890123", "1984", "Orwell", 1900, "English");
		Book book2 = new Book("0011223344556", "What technology wants", "Kelly, Kevin", 2010, "English");
		Book book3 = new Book("9911882277336", "The big book", "Guess", 0, "Hebrew");

		createReview(book1, user1, "Awesome!", 5.0);
		createReview(book2, user1, "Dirt!", 0.0);
		createReview(book3, user1, "Mediocre!", 3.0);
		createReview(book3, user2, "Ok!", 2.0);

		userDao.save(user1);
		userDao.save(user2);

		tx.commit();

		tx.begin();
		List<Book> books = bookDao.fetchAllBooksReviewedByUser(user1.getId());
		assertEquals(books.size() + " books for user found in database", 3, books.size());				
		tx.commit();
	}
}
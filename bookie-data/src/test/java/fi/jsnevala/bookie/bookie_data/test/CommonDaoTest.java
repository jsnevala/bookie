package fi.jsnevala.bookie.bookie_data.test;

import java.util.Date;

import javax.persistence.EntityTransaction;

import org.junit.Test;
import static org.junit.Assert.*;

import fi.jsnevala.bookie.bookie_data.User;

public class CommonDaoTest extends CommonDaoTestCase {

		
	@Test
	public void testPrimaryKeyNotNull() throws Exception {
		EntityTransaction tx = getEntityManager().getTransaction();
		tx.begin();
		
		logTestHeader("Primary key of persisted user is not null");
		User user = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		userDao.save(user);

		assertNotNull("Primary key of user is empty after persist, is transaction ongoing?", user.getId());
		tx.commit();
	}
	
	@Test
	public void testUserFoundInDatabase() throws Exception {
		EntityTransaction tx = getEntityManager().getTransaction();
		tx.begin();
		
		logTestHeader("User is found from database");
		User user = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		userDao.save(user);

		assertNotNull("User not found in database", userDao.fetchWithPrimaryKey(user.getId()));
		tx.commit();
	}
/*
	@Test
	public void testOneUserFound() throws Exception {
		EntityTransaction tx = getEntityManager().getTransaction();
		tx.begin();
		
		logTestHeader("User is found from database");
		User user = new User("jussi2", "Helsinki, Finland", new Date());
		userDao.persist(user);

		assertEquals("More than one user found in database", 1, userDao.fetchAll().size());				
		tx.commit();
	}
*/
	
}

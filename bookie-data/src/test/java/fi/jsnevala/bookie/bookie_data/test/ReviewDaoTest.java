package fi.jsnevala.bookie.bookie_data.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityTransaction;

import org.junit.Test;

import fi.jsnevala.bookie.bookie_data.Book;
import fi.jsnevala.bookie.bookie_data.User;
import fi.jsnevala.bookie.bookie_data.Review;

public class ReviewDaoTest extends CommonDaoTestCase {
	@Test
	public void testFetchAllReviewsByUser() throws Exception {
		EntityTransaction tx = getEntityManager().getTransaction();
		tx.begin();
		
		logTestHeader("Fetch all reviews by user");
		User user1 = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		User user2 = new User("stanley", "NYC", new Date(), "dummypassword");

		Book book1 = new Book("1234567890123", "1984", "Orwell", 1900, "English");
		Book book2 = new Book("0011223344556", "What technology wants", "Kelly, Kevin", 2010, "English");
		Book book3 = new Book("9911882277336", "The big book", "Guess", 0, "Hebrew");

		createReview(book1, user1, "Awesome!", 5.0);
		createReview(book2, user1, "Dirt!", 0.0);
		createReview(book3, user1, "Mediocre!", 3.0);
		createReview(book3, user2, "Ok!", 2.0);

		userDao.save(user1);
		userDao.save(user2);
		tx.commit();

		tx.begin();
		List<Review> reviews = reviewDao.fetchAllReviewsByUser(user1.getId());
		assertEquals(reviews.size() + " reviews for user found in database", 3, reviews.size());				
		tx.commit();
	}


	@Test
	public void testFetchAllReviewsForBook() throws Exception {
		EntityTransaction tx = getEntityManager().getTransaction();
		tx.begin();
		
		logTestHeader("Fetch all reviews for book");
		User user1 = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		User user2 = new User("stanley", "NYC", new Date(), "dummypassword");

		Book book1 = new Book("1234567890123", "1984", "Orwell", 1900, "English");
		Book book2 = new Book("0011223344556", "What technology wants", "Kelly, Kevin", 2010, "English");
		Book book3 = new Book("9911882277336", "The big book", "Guess", 0, "Hebrew");

		createReview(book1, user1, "Awesome!", 5.0);
		createReview(book2, user1, "Dirt!", 0.0);
		createReview(book3, user1, "Mediocre!", 3.0);
		createReview(book3, user2, "Ok!", 2.0);

		userDao.save(user1);
		userDao.save(user2);
		tx.commit();

		tx.begin();
		List<Review> reviews = reviewDao.fetchAllReviewsForBook(book3.getId());
		assertEquals(reviews.size() + " reviews for book found in database", 2, reviews.size());				
		tx.commit();
	}

	@Test
	public void testFetchNLatestReviews() throws Exception {
		EntityTransaction tx = getEntityManager().getTransaction();
		tx.begin();
		
		logTestHeader("Fetch 5 latest reviews");
		User user1 = new User("jussi2", "Helsinki, Finland", new Date(), "dummypassword");
		User user2 = new User("stanley", "NYC", new Date(), "dummypassword");

		Book book1 = new Book("1234567890123", "1984", "Orwell", 1900, "English");
		Book book2 = new Book("0011223344556", "What technology wants", "Kelly, Kevin", 2010, "English");
		Book book3 = new Book("9911882277336", "The big book", "Guess", 0, "Hebrew");
		Book book4 = new Book("8437585874557", "Code complete", "McConnell, Steve", 2004, "English");

		List<Review> refReviews = new ArrayList<Review>();

		Date date1 = new Date();

		final int MINUTE_IN_MS = 1000 * 60;

		userDao.save(user1);

		createReviewWithDate(book1, user1, "Awesome!", 5.0, date1);
		
		refReviews.add(createReviewWithDate(book2, user1, "Dirt!", 0.0, new Date(date1.getTime() + MINUTE_IN_MS)));
		refReviews.add(createReviewWithDate(book3, user2, "Ok!", 2.0, new Date(date1.getTime() + 2 * MINUTE_IN_MS)));
		refReviews.add(createReviewWithDate(book3, user1, "Mediocre!", 3.0, new Date(date1.getTime() + 3 * MINUTE_IN_MS)));
		refReviews.add(createReviewWithDate(book4, user1, "Classic!", 5.0, new Date(date1.getTime() + 4 * MINUTE_IN_MS)));
		refReviews.add(createReviewWithDate(book1, user2, "Poor!", 1.0, new Date(date1.getTime() + 5 * MINUTE_IN_MS)));

		tx.commit();

		Collections.reverse(refReviews);
		
		tx.begin();
		List<Review> reviews = reviewDao.fetchNLatestReviews(5);
		assertTrue("5 latest rewiews mismatch.", reviews.equals(refReviews));				
		tx.commit();
	}
	
/*  TODO testFetchNTopReviews
	@Test
	public void testFetchNTopReviews() throws Exception {
		EntityTransaction tx = getEntityManager().getTransaction();
		tx.begin();
		
		logTestHeader("Fetch all reviews for book");
		User user1 = new User("jussi2", "Helsinki, Finland", new Date());
		User user2 = new User("stanley", "NYC", "2000-08-31");

		Book book1 = new Book("1234567890123", "1984", "Orwell", 1900, "English");
		Book book2 = new Book("0011223344556", "What technology wants", "Kelly, Kevin", 2010, "English");
		Book book3 = new Book("9911882277336", "The big book", "Guess", 0, "Hebrew");

		createReview(book1, user1, 2012, "Awesome!", 5.0);
		createReview(book2, user1, 1900, "Dirt!", 0.0);
		createReview(book3, user1, 3000, "Mediocre!", 3.0);
		createReview(book3, user2, 1000, "Ok!", 2.0);

		userDao.save(user1);
		userDao.save(user2);
		tx.commit();

		tx.begin();
		List<Review> reviews = reviewDao.fetchAllReviewsForBook(book3.getId());
		assertEquals(reviews.size() + " reviews for book found in database", 2, reviews.size());				
		tx.commit();
	}
*/
}
package fi.jsnevala.bookie.bookie_data.test;

import org.junit.Before;

import fi.jsnevala.bookie.bookie_data.UserDaoImplementation;
import fi.jsnevala.bookie.bookie_data.ReviewDaoImplementation;
import fi.jsnevala.bookie.bookie_data.BookDaoImplementation;

public class CommonDaoTestCase extends DatabaseTestCase {

	protected UserDaoImplementation userDao;
	protected ReviewDaoImplementation reviewDao;
	protected BookDaoImplementation bookDao;
	
	/**
	 * Create a concrete DAO-object that handles users, reviews or books and set used entity manager
	 */
	@Before
	public void createDaos() {
		UserDaoImplementation uDao = new UserDaoImplementation();
		ReviewDaoImplementation rDao = new ReviewDaoImplementation();
		BookDaoImplementation bDao = new BookDaoImplementation();
		
		uDao.setEntityManager(getEntityManager());
		rDao.setEntityManager(getEntityManager());
		bDao.setEntityManager(getEntityManager());

		userDao = uDao;
		reviewDao = rDao;
		bookDao = bDao;
	}

}

package fi.jsnevala.bookie.bookie_data;

import java.util.List;

import javax.persistence.Query;

/**
 * 
 * Class that implements review DAO
 *
 */
public class ReviewDaoImplementation extends CommonDaoBase<Review> implements
		ReviewDao {

	/**
	 * Constructs review DAO
	 */
	public ReviewDaoImplementation() {
		super(Review.class);
	}

	/**
	 * Returns all reviews by user
	 * @param userId ID of the user
	 * @return List of reviews by a user
	 */
	public List<Review> fetchAllReviewsByUser(Integer userId) {
		String query = "SELECT r FROM Review r WHERE r.reviewer.id = :id";
		Query q = entityManager.createQuery(query);
		q.setParameter("id", userId);
		@SuppressWarnings("unchecked")
		List<Review> reviews = q.getResultList();
		return reviews;		
	}

	/**
	 * Returns all reviews for a book
	 * @param bookId ID of the book
	 * @return List of reviews for a book
	 */
	public List<Review> fetchAllReviewsForBook(Integer bookId) {
		String query = "SELECT r FROM Review r WHERE r.book.id = :id";
		Query q = entityManager.createQuery(query);
		q.setParameter("id", bookId);
		@SuppressWarnings("unchecked")
		List<Review> reviews = q.getResultList();
		return reviews;		
	}

	/**
	 * Returns N reviews with highest rating
	 * @param n Number of reviews to be returned
	 * @return List of highest rated reviews
	 */
	public List<Review> fetchNTopReviews(Integer n) {
		// TODO fetchNTopReviews 
		return null;		
	}

	/**
	 * Returns N latest reviews
	 * @param n Number of reviews to be returned
	 * @return List of latest reviews
	 */
	public List<Review> fetchNLatestReviews(Integer n) {
		String query = "SELECT r FROM Review r ORDER BY r.created DESC";
		Query q = entityManager.createQuery(query);
		q.setMaxResults(n);
		@SuppressWarnings("unchecked")
		List<Review> reviews = q.getResultList();
		return reviews;		
	}

}

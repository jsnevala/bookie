package fi.jsnevala.bookie.bookie_data;

import java.util.List;

/**
 * 
 * Interface for user DAO
 *
 */
public interface UserDao extends CommonDao<User> {
	/**
	 * Returns all users that have reviewed a book
	 * @param bookId Id of the book
	 * @return List of users that have reviewed a book
	 */
	public List<User> fetchAllUsersByBook(Integer bookId);
	
	/**
	 * Persist user
	 * @param user User to be persisted
	 */
	public void save(User user);
}


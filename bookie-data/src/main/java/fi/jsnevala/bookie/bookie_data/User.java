package fi.jsnevala.bookie.bookie_data;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.constraints.Length;

/**
 * Class to model a user in the service
 */
@Entity
@Table(name = "user")
public class User extends EntityBase implements Serializable {

	/**
	 * Version for serialize
	 */
	private static final long serialVersionUID = -7699341844061089537L;

	/**
	 * Version for persistence
	 */
	@Version
	private int version = 0;

	/**
	 * Minimum length of username
	 */
	public static final int MIN_USERNAME_LENGTH = 2;

	/**
	 * Maximum length of username
	 */
	public static final int MAX_USERNAME_LENGTH = 255;
	
	/**
	 * Username
	 */
	private String username = null;
	
	
	/**
	 * Minimum length of password
	 */
	public static final int MIN_PASSWORD_LENGTH = 4;

	/**
	 * Maximum length of password
	 */
	public static final int MAX_PASSWORD_LENGTH = 255;

	/**
	 * Password
	 */
	private String password = null;
	
	/**
	 * Minimum length of authority
	 */
	public static final int MIN_AUTHORITY_LENGTH = 2;
	
	/**
	 * Minimum length of authority
	 */
	public static final int MAX_AUTHORITY_LENGTH = 255;
	
	/**
	 * User authority (ROLE_USER etc)
	 */
	private String authority = null;
	
	/**
	 * User account enabled/disabled
	 */
	private Boolean enabled = null;
	
	/**
	 * Minimum length of location
	 */
	public static final int MIN_LOCATION_LENGTH = 2;	
	
	/**
	 * Maximum length of location
	 */
	public static final int MAX_LOCATION_LENGTH = 255;	
	
	/**
	 * User's location (city, country etc)
	 */
	private String location = null;
	
	/** 
	 * Date of joining the service
	 */
	private Date joinDate = null;

	/**
	 * Books that the user has reviewed
	 */
	private Set<Review> reviews = null;
		
	/**
	 * Constructs new user
	 */
	public User() {
		setAuthority("ROLE_USER");
		setEnabled(true);
	}

	/**
	 * Constructs new user
	 */
	public User(String username, String location, Date joinDate, String password) {
		setUsername(username.trim());
		setLocation(location.trim());
		setJoinDate(joinDate);
		setPassword(password);
		setAuthority("ROLE_USER");
		setEnabled(true);
		reviews = new HashSet<Review>();
	}

	/**
	 * Returns reviews by user
	 * @return List of reviews by user
	 */
	@OneToMany(mappedBy = "reviewer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@ForeignKey(name = "user_review_fk")
	public Set<Review> getReviews() {
		return reviews;
	}

	/**
	 * Sets reviews 
	 * @param reviews Reviews for user
	 */
	public void setReviews(Set<Review> reviews) {
		this.reviews = reviews;
	}

	/**
	 * Adds a review for user
	 * @param review Review to be added
	 * @throws NullPointerException
	 */
	public void addReview(Review review) throws NullPointerException {
		review.setReviewer(this);
		reviews.add(review);
	}

	/**
	 * Get username
	 * @return Username
	 */
	@NotNull
	@Length(min = MIN_USERNAME_LENGTH, max = MAX_USERNAME_LENGTH) 
	@Column(name = "username", length = MAX_USERNAME_LENGTH, nullable = false, unique = true)
	public String getUsername() {
		return username;
	}

	/**
	 * Set username
	 * @param username Username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Get location
	 * @return Location
	 */
	@NotNull
	@Length(min = MIN_LOCATION_LENGTH, max = MAX_LOCATION_LENGTH)
	@Column(name = "location", length = MAX_LOCATION_LENGTH, nullable = false)
	public String getLocation() {
		return location;
	}

	/**
	 * Set location
	 * @param location Location
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * Get join date
	 * @return Joining date
	 */
	@NotNull
	@Column(name = "join_date", nullable = false)
	public Date getJoinDate() {
		return joinDate;
	}

	/**
	 * Set join date
	 * @param joinDate Date of joining the service
	 */
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	/**
	 * Gets the password for user
	 * @return the password
	 */
	@NotNull
	@Length(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH) 
	@Column(name = "password", nullable = false)
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password for user
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Return the authority for user
	 * @return the authority
	 */
	@NotNull
	@Column(name = "authority", nullable = false)
	public String getAuthority() {
		return authority;
	}

	/**
	 * Sets the authority for user
	 * @param authority the auhority to set
	 */
	public void setAuthority(String authority) {
		this.authority = authority;
	}

	/**
	 * Return user account status (enabled/disabled)
	 * @return the enabled
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	/**
	 * Sets user account status (enabled/disabled)
	 * @param enabled the enabled to set
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	/**
	 * String representation of user
	 * @return String
	 */
	@Override
	public String toString() {
		return this.username + ";" + this.location + ";" + this.joinDate; 
	}
	
	/**
	 * Comparator for users
	 */
	public static Comparator<User> UserComparator = new Comparator<User>() {
		/**
		 * @param user1 User to compare
		 * @param user2 User to compare
		 * @return Comparison result
		 */
		public int compare(User user1, User user2) {
			String username1 = user1.getUsername().toUpperCase();
			String username2 = user2.getUsername().toUpperCase();
			return username1.compareTo(username2);
		}
	};
}

package fi.jsnevala.bookie.bookie_data;

import java.util.List;

import javax.persistence.Query;

/**
 * 
 * Class that implements book DAO
 *
 */
public class BookDaoImplementation extends CommonDaoBase<Book> implements
		BookDao {

	/**
	 * Constructs book DAO
	 */
	public BookDaoImplementation() {
		super(Book.class);
	}

	/**
	 * Returns all books reviewed by a user
	 * @param userId
	 * @return List of books reviewed by a user
	 */
	public List<Book> fetchAllBooksReviewedByUser(Integer userId) {
		String query = "SELECT r.book FROM Review r WHERE r.reviewer.id = :id";
		Query q = entityManager.createQuery(query);
		q.setParameter("id", userId);
		@SuppressWarnings("unchecked")
		List<Book> books = q.getResultList();
		return books;		
	}

}

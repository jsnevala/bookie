package fi.jsnevala.bookie.bookie_data;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.constraints.Length;

/**
 * This class models a review
 */
@Entity
@Table(name = "review")
public class Review extends EntityBase implements Serializable {
		
	/**
	 * Version for serialize
	 */
	private static final long serialVersionUID = -6900132070520009874L;

	/**
	 * Version for persistence
	 */
	@Version
	private int version = 0;
	
	/**
	 * Reviewed book
	 */
	private Book book = null;
	
	/**
	 * User that has read the book
	 */
	private User reviewer = null;

	/**
	 *  Max length of comments
	 * TODO: check if longer comments are ok
	 */
	public static final int MAX_COMMENTS_LENGTH = 64000;  

	/**
	 * Comments for the book
	 */
	private String comments = null;

	/**
	 * Minimum value for rating
	 */
	public static final Double MIN_RATING = 0.0;  
	
	/**
	 * Maximum value for rating
	 */
	public static final Double MAX_RATING = 5.0;  

	/**
	 * Rating
	 */
	private Double rating = null;

	/**
	 * Date of submitting the review
	 */
	private Date created = null;
		
	/**
	 * Constructs a new review
	 */
	public Review() {
		
	}

	/**
	 * Constructs a new review
	 */
	public Review(String comments, Double rating, Date created) throws IllegalArgumentException {
		setBook(null);
		setReviewer(null);
		setComments(comments.trim());
		setRating(rating);
		setCreated(created);
	}
	
	/**
	 * String representation for review
	 * @return String
	 */
	@Override
	public String toString() {
		return book.getTitle() + "; " + reviewer.getUsername() + "; " + comments + 
			   "; " + rating + "; " + created;
	}
	
	/**
	 * Returns the reviewed book
	 * @return the reviewed book
	 */
	@ManyToOne(cascade = CascadeType.ALL)
	public Book getBook() {
		return book;
	}

	/**
	 * Sets the reviewed book
	 * @param book the book to set
	 */
	public void setBook(Book book) {
		this.book = book;
	}

	/**
	 * Returns the reviewer
	 * @return the reviewer
	 */
	@ManyToOne
	@ForeignKey(name = "review_user_fk")
	public User getReviewer() {
		return reviewer;
	}

	/**
	 * Returns the date of submitting the review
	 * @return Date of submitting the review
	 */
	@NotNull
	@Column(name = "created", nullable = false)
	public Date getCreated() {
		return created;
	}

	/**
	 * Set time and date of submitting the review
	 * @param created Date of submitting the review
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * Sets the reviewer
	 * @param reviewer the reviewer to set
	 */
	public void setReviewer(User reviewer) {
		this.reviewer = reviewer;
	}

	/**
	 * Returns comments for the review
	 * @return Comments for the review
	 */
	@NotNull
	@Length(min = 0, max = MAX_COMMENTS_LENGTH)
	@Column(name = "comments", length = MAX_COMMENTS_LENGTH, nullable = false)
	public String getComments() {
		return comments;
	}

	/**
	 * Sets comments for the review
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Returns rating for the review
	 * @return the rating
	 */
	@NotNull
	@Column(name = "rating", nullable = false)
	public Double getRating() {
		return rating;
	}

	/**
	 * Sets rating and checks that it is valid
	 * @param rating the rating to set
	 * @throws IllegalArgumentException
	 */
	public void setRating(Double rating) throws IllegalArgumentException {
		if (rating != null) {
			if ((rating < MIN_RATING) || (rating > MAX_RATING)) {
				throw new IllegalArgumentException("Rating must be between 0.0 and 5.0, with 0.5 steps (was "  + rating + ").");
			} 
			
			Double testRating = rating;
			while (testRating > MIN_RATING) {
				testRating -= 0.5;
			}
			
			if (testRating < MIN_RATING ) {
				throw new IllegalArgumentException("Rating must be between 0 and 5, with 0.5 steps (was "  + rating + ").");
			}
		}
		this.rating = rating;
	}

	
	/**
	 * Comparator for reviews
	 */
	public static Comparator<Review> ReviewComparator = new Comparator<Review>() {
		/**
		 * @param review1 Review to compare
		 * @param review2 Review to compare
		 * @return Comparison result
		 */
		public int compare(Review review1, Review review2) {
			String reviewBook1 = review1.getBook().getTitle().toUpperCase();
			String reviewBook2 = review2.getBook().getTitle().toUpperCase();
			return reviewBook1.compareTo(reviewBook2);
		}
		
	};
}

package fi.jsnevala.bookie.bookie_data;

import java.util.List;

/**
 * 
 * Interface for review DAO
 *
 */
public interface ReviewDao extends CommonDao<Review> {
	/**
	 * Returns all reviews by user
	 * @param userId ID of the user
	 * @return List of reviews by a user
	 */
	public List<Review> fetchAllReviewsByUser(Integer userId);
	
	/**
	 * Returns all reviews for a book
	 * @param bookId ID of the book
	 * @return List of reviews for a book
	 */
	public List<Review> fetchAllReviewsForBook(Integer bookId);

	/**
	 * Returns N reviews with highest rating
	 * @param n Number of reviews to be returned
	 * @return List of highest rated reviews
	 */
	public List<Review> fetchNTopReviews(Integer n);

	/**
	 * Returns N latest reviews
	 * @param n Number of reviews to be returned
	 * @return List of latest reviews
	 */
	public List<Review> fetchNLatestReviews(Integer n);
}

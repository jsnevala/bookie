package fi.jsnevala.bookie.bookie_data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * 
 * Implementation common to all DAOs
 *
 * @param <E> entity
 */
public abstract class CommonDaoBase<E extends EntityBase> implements CommonDao<E> {

	/**
	 * Entity class
	 */
	private Class<E> entityClass;
	
	/**
	 * EntityManager
	 */
	@PersistenceContext(name="bookiePersistence")
	protected EntityManager entityManager;
	
	/**
	 * Constructs common DAO
	 * @param entityClass
	 */
	public CommonDaoBase(final Class<E> entityClass) {
		this.entityClass = entityClass;
	}

	/**
	 * Returns the entity class
	 * @return the entityClass
	 */
	public Class<E> getEntityClass() {
		return entityClass;
	}

	/**
	 * Sets the entity class
	 * @param entityClass the entityClass to set
	 */
	public void setEntityClass(Class<E> entityClass) {
		this.entityClass = entityClass;
	}

	/**
	 * Returns the entity manager
	 * @return the entityManager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Sets entity manager
	 * @param entityManager the entityManager to set
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Fetches all objects of type that is requested
	 * @return List of queried objects
	 */
	public List<E> fetchAll() {
		Query q = entityManager.createQuery("SELECT o FROM " + entityClass.getName() + " AS o");
		@SuppressWarnings("unchecked")
		List<E> results = q.getResultList();
		return results;
	}
	
	/**
	 * Fetches an object with requested primary key
	 * @param id primary key
	 * @return Queried entity with given primary key
	 */
	public E fetchWithPrimaryKey(final Integer id) {
		return entityManager.find(entityClass, id);
	}
}

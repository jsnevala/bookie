package fi.jsnevala.bookie.bookie_data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Common functions for all entities
 */
@MappedSuperclass
public abstract class EntityBase implements Serializable {
	
	/**
	 * Version for serialize 
	 */
	private static final long serialVersionUID = -4138711006277155383L;

	/**
	 * Primary key
	 */
	private Integer id = null;

	/**
	 * Get  id
	 * @return primary key of object
	 */
	@Id()
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	/**
	 * Set id (used by hibernate only)
	 * @param id primary key to set
	 */
	protected void setId(Integer id) {
		this.id = id;
	}
}

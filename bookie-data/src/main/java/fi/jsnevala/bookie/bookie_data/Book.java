package fi.jsnevala.bookie.bookie_data;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.constraints.Length;

/**
 * This class models a book
 *
 */
@Entity
@Table(name = "book")
public class Book extends EntityBase implements Serializable {
	
	/**
	 * Version for serialize
	 */
	private static final long serialVersionUID = -1295271346400118752L;

	/**
	 * Version for persistence
	 */
	@Version
	private int version = 0;

	/**
	 * ISBN minimum length
	 */
	public static final int MIN_ISBN_LENGTH = 10;

	/**
	 * ISBN maximum length
	 */
	public static final int MAX_ISBN_LENGTH = 13;

	/**
	 * ISBN
	 */
	private String isbn = null;

	/**
	 * Title minimum length
	 */
	public static final int MIN_TITLE_LENGTH = 1;

	/**
	 * Title maximum length
	 */
	public static final int MAX_TITLE_LENGTH = 255;

	/**
	 * Title
	 */
	private String title = null;
	
	/**
	 * Author minimum length
	 */
	public static final int MIN_AUTHOR_LENGTH = 1;
	
	/**
	 * Author maximum length
	 */
	public static final int MAX_AUTHOR_LENGTH = 255;

	/**
	 * Author
	 */
	private String author = null;
	
	/**
	 * Publish year
	 */
	private Integer year = null;

	/**
	 * Language minimum length of string
	 */
	public static final int MIN_LANGUAGE_LENGTH = 1;
	
	/**
	 * Language maximum length of string
	 */
	public static final int MAX_LANGUAGE_LENGTH = 255;

	/**
	 * Language
	 */
	private String language = null;

	
	/**
	 * Reviews for book
	 */
	private Set<Review> reviews = null;
	
	/**
	 * Constructs a new book
	 */
	public Book() {
		
	}

	/**
	 * Constructs a new book
	 * @param title Book title
	 * @param author Book author
	 * @param year Publish year
	 * @param language Language of the book
	 */
	public Book(String title, String author, Integer year, String language) {
		this("0000000000000", title, author, year, language);
	}

	/**
	 * Constructs a new book
	 * @param isbn ISBN of the book
	 * @param title Book title
	 * @param author Book author
	 * @param year Publish year
	 * @param language Language of the book
	 */
	public Book(String isbn, String title, String author, Integer year, String language) {
		setISBN(isbn.trim());
		setTitle(title.trim());
		setAuthor(author.trim());
		setYear(year);
		setLanguage(language.trim());
		reviews = new HashSet<Review>();
	}

	/**
	 * Returns the review for book
	 * @return reviews
	 */
	@OneToMany(mappedBy = "book", fetch = FetchType.EAGER)
	@ForeignKey(name = "book_review_fk")
	public Set<Review> getReviews() {
		return reviews;
	}

	/**
	 * Sets the review for book
	 * @param reviews Reviews for book
	 */
	public void setReviews(Set<Review> reviews) {
		this.reviews = reviews;
	}

	/**
	 * Adds a review for book
	 * @param review Review
	 * @throws NullPointerException
	 */
	public void addReview(Review review) throws NullPointerException {
		review.setBook(this);
		reviews.add(review);
	}

	/**
	 * Returns ISBN
	 * @return ISBN of the book
	 */
	@Length(min = MIN_ISBN_LENGTH, max = MAX_ISBN_LENGTH) 
	@Column(name = "isbn", length = MAX_ISBN_LENGTH, nullable = false)
	public String getISBN() {
		return isbn;
	}

	/**
	 * Set ISBN of the book
	 * @param isbn the ISBN to set
	 */
	public void setISBN(String isbn) {
		this.isbn = isbn;
	}

	/**
	 * Returns book title
	 * @return the title
	 */
	@NotNull
	@Length(min = MIN_TITLE_LENGTH, max = MAX_TITLE_LENGTH) 
	@Column(name = "title", length = MAX_TITLE_LENGTH, nullable = false)
	public String getTitle() {
		return title;
	}

	/**
	 * Sets book title
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Returns author of the book
	 * @return the author
	 */
	@NotNull
	@Length(min = MIN_AUTHOR_LENGTH, max = MAX_AUTHOR_LENGTH) 
	@Column(name = "author", length = MAX_AUTHOR_LENGTH, nullable = false)
	public String getAuthor() {
		return author;
	}

	/**
	 * Sets author of the book
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * Returns publish year of the book
	 * @return the year
	 */
	@NotNull
	@Column(name = "year", nullable = false)
	public Integer getYear() {
		return year;
	}

	/**
	 * Sets publish year of the book
	 * @param year the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	/**
	 * Returns the language of the book
	 * @return the language
	 */
	@NotNull
	@Length(min = MIN_LANGUAGE_LENGTH, max = MAX_LANGUAGE_LENGTH) 
	@Column(name = "language", length = MAX_LANGUAGE_LENGTH, nullable = false)
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets language of the book
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * String representation of the book
	 * @return String
	 */
	@Override
	public String toString() {
		return getTitle() + "; " + getAuthor() + "; " + getYear() + "; " + getLanguage() + "; " + getISBN();
	}

	/**
	 * Comparator for books
	 */
	public static Comparator<Book> BookComparator = new Comparator<Book>() {
		/**
		 * @param book1 Book to compare
		 * @param book2 Book to compare
		 * @return Comparison result
		 */
		public int compare(Book book1, Book book2) {
			String title1 = book1.getTitle().toUpperCase();
			String title2 = book2.getTitle().toUpperCase();
			return title1.compareTo(title2);
		}
		
	};
}


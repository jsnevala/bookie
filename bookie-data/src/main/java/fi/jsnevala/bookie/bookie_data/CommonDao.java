package fi.jsnevala.bookie.bookie_data;

import java.util.List;

/**
 * Common interface for all DAO interfaces 
 */
public interface CommonDao<E extends EntityBase> {

	/**
	 * @return Collection of queried objects
	 */
	public List<E> fetchAll();
	
	/**
	 * @param id primary key
	 * @return Queried entity with given primary key
	 */
	public E fetchWithPrimaryKey(final Integer id);	
	
}

package fi.jsnevala.bookie.bookie_data;

import java.util.List;

import javax.persistence.Query;

/**
 * 
 * Class that implements user DAO
 *
 */
public class UserDaoImplementation extends CommonDaoBase<User> implements UserDao {

	/**
	 * Constructs new user DAO
	 */
	public UserDaoImplementation() {
		super(User.class);
	}
	
	/**
	 * Returns all users that have reviewed a book
	 * @param bookId Id of the book
	 * @return List of users that have reviewed a book
	 */
	public List<User> fetchAllUsersByBook(Integer bookId) {
		String query = "SELECT r.reviewer FROM Review r WHERE r.book.id = :id";
		Query q = entityManager.createQuery(query);
		q.setParameter("id", bookId);
		@SuppressWarnings("unchecked")
		List<User> users = q.getResultList();
		return users;		
	}
	
	/**
	 * Persist user
	 * @param user User to be persisted
	 */
	public void save(User user) {
		if (user.getId() == null) {
			entityManager.persist(user);
		} else {
			entityManager.merge(user);
		}
	}

}


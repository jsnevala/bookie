package fi.jsnevala.bookie.bookie_data;

import java.util.List;

/**
 * 
 * Interface for book DAO
 *
 */
public interface BookDao extends CommonDao<Book> {
	/**
	 * Returns all books reviewed by a user
	 * @param userId
	 * @return List of books reviewed by a user
	 */
	public List<Book> fetchAllBooksReviewedByUser(Integer userId);
}

insert into book (id, isbn, author, language, title, year) values (1, '0011223344556', 'Kelly, Kevin', 'English', 'What technology wants', '2010');
insert into book (id, isbn, author, language, title, year) values (2, '9843759845598', 'Seibel', 'English', 'Coders at Work', '2010');
insert into book (id, isbn, author, language, title, year) values (3, '7209431091022', 'McConnel Steve', 'English', 'Code complete 2', '2010');
insert into book (id, isbn, author, language, title, year) values (4, '1092398545776', 'Kuha, Janne', 'English', 'Tehokas Java EE-sovellustuotanto', '2010');
insert into book (id, isbn, author, language, title, year) values (5, '0129834437557', 'Tolkien, JRR', 'English', 'LOTR', '2010');
insert into book (id, isbn, author, language, title, year) values (6, '3454577689853', 'Meyer, Michael', 'Finnish', 'Ihmisten vuosi 1989', '2010');
insert into book (id, isbn, author, language, title, year) values (7, '9823476943455', 'Hobsbawm, Eric', 'Finnish', 'Äärimmäisyyksien aika', '1999');
insert into book (id, isbn, author, language, title, year) values (8, '2343455556564', 'Herbert, Frank', 'English', 'Dune', '1965');

insert into user (id, join_date, location, username, password, authority, enabled) values (1, '2012-08-29 11:00:40.294', 'Helsinki, Finland', 'jussi', '937a542ee7cff0b25c4bf251723c751a80c67b10', 'ROLE_USER', '1');
insert into user (id, join_date, location, username, password, authority, enabled) values (2, '2012-09-29 11:00:40.541', 'Guest Town', 'guest', '225f8735e27294f150ed51ad0a9dbc586cdeedc3', 'ROLE_USER', '1');
insert into user (id, join_date, location, username, password, authority, enabled) values (3, '2012-08-29 11:00:40.552', 'SF', 'george', '0fcb7a0c37efbb449100aaa932ff08cedb4bdf69', 'ROLE_USER', '1');
insert into user (id, join_date, location, username, password, authority, enabled) values (4, '2012-09-29 11:00:40.556', 'Washington DC', 'john', 'f4fc58814dec3ba10909ad1d1571420b104fe70b', 'ROLE_USER', '1');
insert into user (id, join_date, location, username, password, authority, enabled) values (5, '2012-10-29 11:00:40.562', 'Philadelphia', 'phil', '26a05c8978002593f3c9f6c232ed5e65d5782054', 'ROLE_USER', '1');

insert into review (id, comments, created, rating, book_id, reviewer_id) values (1, 'Intriguing pondering about technology and how we use and perceive it.', '2012-09-15 01:00:40.567', 5.0, 1, 1);
insert into review (id, comments, created, rating, book_id, reviewer_id) values (2, 'Interesting insights into the minds of some of the most widely known programmers.', '2012-09-15 02:00:40.567', 4.0, 2, 1);
insert into review (id, comments, created, rating, book_id, reviewer_id) values (3, 'A must read.', '2012-09-15 11:00:40.567', 4.5, 3, 1);
insert into review (id, comments, created, rating, book_id, reviewer_id) values (4, 'Good starting point for all Java EE-experts-to-be.', '2012-09-15 11:00:40.567', 4.0, 4, 1);
insert into review (id, comments, created, rating, book_id, reviewer_id) values (5, 'Excellent.', '2012-09-15 11:00:40.567', 4.0, 5, 1);
insert into review (id, comments, created, rating, book_id, reviewer_id) values (6, 'not so good', '2012-06-01 11:00:40.567', 2.0, 1, 2);
insert into review (id, comments, created, rating, book_id, reviewer_id) values (7, 'problematic', '2012-06-12 11:00:40.567', 3.5, 3, 2);
insert into review (id, comments, created, rating, book_id, reviewer_id) values (8, 'classic', '2012-07-13 11:00:40.567', 4.5, 5, 2);
insert into review (id, comments, created, rating, book_id, reviewer_id) values (9, 'would not recommend to anyone', '2012-01-21 11:00:40.567', 0.5, 5, 3);
insert into review (id, comments, created, rating, book_id, reviewer_id) values (10, 'a must read', '2012-09-21 11:00:40.567', 4.5, 7, 4);
insert into review (id, comments, created, rating, book_id, reviewer_id) values (11, 'i liked a lot', '2012-03-03 11:00:40.567', 4.0, 6, 4);
insert into review (id, comments, created, rating, book_id, reviewer_id) values (12, 'for scifiheads', '2012-02-02 11:00:40.567', 4.5, 8, 4);

